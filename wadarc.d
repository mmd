/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module wadarc;
private:

import std.stdio;


// ////////////////////////////////////////////////////////////////////////// //
public struct PakFileInfo {
  string name;
  uint size;
}


// ////////////////////////////////////////////////////////////////////////// //
private struct WadFileInfo {
  uint ofs;
  uint size;
  string name;
  File wadfl;
}


__gshared WadFileInfo[string] files;
__gshared bool firstWad = true;


// ////////////////////////////////////////////////////////////////////////// //
public void forEachWadFile (void delegate (string name, uint size) dg) {
  foreach (const ref fi; files.byValue) dg(fi.name, fi.size);
}


// ////////////////////////////////////////////////////////////////////////// //
public ubyte[] wadLoadFile (const(char)[] name) {
  if (auto fi = name in files) {
    if (fi.size > 1024*1024*8) throw new Exception("file too big");
    if (fi.size == 0) return null;
    auto res = new ubyte[](fi.size);
    uint pos = 0;
    fi.wadfl.seek(fi.ofs);
    while (pos < res.length) {
      //writeln(name, ": pos=", pos, "; len=", res.length);
      auto rd = fi.wadfl.rawRead(res[pos..$]);
      if (rd.length == 0) throw new Exception("read error ("~name.idup~")");
      pos += cast(uint)rd.length;
    }
    return res;
  }
  throw new Exception("file not found");
}


// ////////////////////////////////////////////////////////////////////////// //
uint readUInt (File fl) {
  uint v;
  foreach (immutable shift; 0..4) {
    ubyte b;
    if (fl.rawRead((&b)[0..1]).length != 1) throw new Exception("read error");
    v |= b<<(shift*8);
  }
  return v;
}

void readBuf(T) (File fl, T[] buf) {
  if (buf.length == 0) return;
  if (buf.length > int.max/2) assert(0, "wtf?!");
  int pos = 0;
  while (pos < buf.length) {
    auto rd = fl.rawRead(buf[pos..$]);
    if (rd.length == 0) throw new Exception("read error");
    pos += cast(uint)rd.length;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void registerWad (string pak) {
  char[4] sign;
  auto fl = File(pak);
  fl.readBuf(sign[]);
  if (firstWad) {
    if (sign != "IWAD") throw new Exception("invalid wad");
    firstWad = false;
  } else {
    if (sign != "PWAD") throw new Exception("invalid wad");
  }
  uint fcount = fl.readUInt;
  uint dofs = fl.readUInt;
  if (fcount == 0) return;
  if (fcount > 1024) throw new Exception("invalid wad");
  fl.seek(dofs);
  while (fcount--) {
    char[8] nbuf = 0;
    uint ofs = fl.readUInt;
    uint size = fl.readUInt;
    fl.readBuf(nbuf[]);
    const(char)[] nm;
    foreach (immutable idx, ref char ch; nbuf[]) {
      if (ch == 0) break;
      if (ch >= 'A' && ch <= 'Z') ch += 32;
      nm = nbuf[0..idx+1];
    }
    string name = nm.idup;
    files[name] = WadFileInfo(ofs, size, name, fl);
  }
}

/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module mminer;

import arsd.simpledisplay;

import engine;
import x11gfx;
import wadarc;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  //vfsAddPak("data/mminer.wad");
  registerWad("data/mminer.wad");

  for (int idx = 1; idx < args.length; ++idx) {
    if (args[idx] == "-file") {
      if (args.length-idx < 2) assert(0, "arg?!");
      registerWad(args[idx+1]);
      ++idx;
    } else {
      assert(0, "invalid arg");
    }
  }

  loadGameData();

  Room curRoom;

  void loadRoom (int idx) {
    curRoom = gameRooms[idx];
    curRoom.initRoom();
    blitType = BlitType.Green;
    curRoom.willyDead = true;
  }

  void restartRoom () {
    auto kst = curRoom.saveKeyState();
    loadRoom(curRoom.roomIdx);
    blitType = BlitType.Normal;
    curRoom.willyDead = false;
    curRoom.restoreKeyState(kst);
  }
  //loadRoom(0);

  auto gfxFill0 = buildImageMasked(gameData["fill"][], 16, 24);
  auto gfxFill1 = buildImageMasked(gameData["fill"][384..$], 72, 24);
  auto gfxAir = buildImageMasked(gameData["air"], 256, 8);
  auto gfxSun = buildImageMasked(gameData["sun"], 24, 16);
  auto gfxFinal = buildImageMasked(gameData["final"], 256, 64);

  auto sdwin = x11gfxInit("Manic Miner");
  bool doQuit;
  X11Image roomImg;
  bool inTitle = true;

  sdwin.eventLoop(1000/20,
    // timer
    delegate () {
      if (sdwin.closed) return;

      if (!inTitle) {
        if (!curRoom.willyDead && !singleStepWait) {
          curRoom.stepGame();
          if (curRoom.willyDead) {
            blitType = BlitType.BlackWhite;
          } else {
            if (curRoom.checkExit()) {
              if (gameRooms.length-curRoom.roomIdx > 1) {
                loadRoom(curRoom.roomIdx+1);
                curRoom.willyDead = true;
              }
              blitType = BlitType.Green;
            }
          }
          singleStepWait = singleStep;
        }
      }

      if (!inTitle) {
        clear(0);
        roomImg = curRoom.draw(roomImg);
        roomImg.blitFast(0, 0);
      } else {
        clear(palcol(240));

        static struct Sun {
          int y;
          ubyte m, h;
        }
        static Sun sun;
        sun.m = 1;
        sun.y = 32;
        sun.h = 16;

        void drawSun () {
          for (int y = sun.h-1; y >= 0; --y) {
            for (int x = 23; x >= 0; --x) {
              ubyte data = gameData["sun"][(y*24)+x];
              if (data) setPixel8(60+x, sun.y+y, data);
            }
          }
        }
        gfxFinal.blitFast(0, 0);
        //sunSpr.blitTo(img, 60, 32);
        drawSun();
        gfxFill0.blitFast(152, 40);
        gfxFill1.blitFast(176, 40);
        //printAt(154, 42, "Hello!", 13);

        static int TITLEwf = 2;
        static int TITLEwp = 0;

        if (++TITLEwp == 16) {
          TITLEwp = 0;
          TITLEwf = (TITLEwf+2)&7;
        }
        drawWilly4(TITLEwf+4);
        // levelset: 122
      }

      realizeVBuf();
      x11gfxBlit();
    },
    // keyboard
    delegate (KeyEvent evt) {
      if (sdwin.closed) return;
      switch (evt.key) {
        case Key.Left: curRoom.keyLeft = evt.pressed; break;
        case Key.Right: curRoom.keyRight = evt.pressed; break;
        case Key.Up: curRoom.keyUp = evt.pressed; break;
        case Key.Down: curRoom.keyDown = evt.pressed; break;
        case Key.Ctrl: curRoom.keyJump = evt.pressed; break;
        case Key.N1: debugColdet = !debugColdet; break;
        case Key.N0:
          if (!evt.pressed) break;
          curRoom.cheatRemoveKeys();
          break;
        case Key.D:
          if (!evt.pressed) break;
          debugColdet = !debugColdet;
          break;
        case Key.S:
          if (!evt.pressed) break;
          singleStep = !singleStep;
          singleStepWait = singleStep;
          break;
        case Key.Space:
          if (!evt.pressed) break;
          if (inTitle) {
            inTitle = false;
            loadRoom(0);
          }
          break;
        case Key.Enter:
          if (!evt.pressed) break;
          if (inTitle) goto case Key.Space;
          if (curRoom.willyDead) { restartRoom(); break; }
          if (singleStep) { singleStepWait = false; break; }
          break;
        case Key.R:
          if (!evt.pressed) break;
          blitType = BlitType.Normal;
          if (curRoom.willyDead) { restartRoom(); break; }
          break;
        case Key.Q: case Key.Escape: doQuit = true; break;
        case Key.Minus:
          if (!evt.pressed) break;
          if (curRoom.roomIdx > 0) {
            loadRoom(curRoom.roomIdx-1);
          }
          break;
        case Key.Plus:
          if (!evt.pressed) break;
          if (gameRooms.length-curRoom.roomIdx > 1) {
            loadRoom(curRoom.roomIdx+1);
          }
          break;
        default: break;
      }
      if (doQuit) { sdwin.close(); return; }
    },
    // mouse
    delegate (MouseEvent evt) {
      if (sdwin.closed) return;
    },
    // char
    delegate (dchar ch) {
      if (sdwin.closed) return;
    },
  );
  x11gfxDeinit();
}

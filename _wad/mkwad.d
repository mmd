import std.file;
import std.path;
import iv.vfs.io;


struct FileInfo {
  uint ofs, size;
  char[8] name = 0;
}


void main (string[] args) {
  if (args.length != 2) {
    writeln("usage: mkwad dir");
    return;
  }

  auto fo = VFile(args[1].baseName~".wad", "w");

  if (args[1].baseName == "mminer") {
    fo.rawWriteExact("IWAD");
  } else {
    fo.rawWriteExact("PWAD");
  }
  fo.writeNum!uint(0); // file count
  fo.writeNum!uint(0); // dir offset

  FileInfo[] files;

  ubyte[1024] buf;

  foreach (DirEntry de; dirEntries(args[1], SpanMode.shallow)) {
    import std.path;
    import std.string;
    string name;
    foreach (char ch; de.name.baseName.stripExtension) {
      if (ch >= 'a' && ch <= 'z') ch -= 32;
      name ~= ch;
    }
    //if (name.indexOf('.') >= 0) continue;
    if (!de.name.isFile) continue;
    if (name.length > 8) continue;
    writeln(name);
    {
      auto fi = VFile(de.name);
      FileInfo ff;
      ff.name[0..name.length] = name[];
      ff.ofs = cast(uint)fo.tell;
      ff.size = cast(uint)fi.size;
      for (;;) {
        auto rd = fi.rawRead(buf[]);
        if (rd.length == 0) break;
        fo.rawWriteExact(rd);
      }
      files ~= ff;
    }
  }

  auto dofs = cast(uint)fo.tell;
  // write dir
  foreach (const ref fi; files) {
    fo.writeNum!uint(fi.ofs);
    fo.writeNum!uint(fi.size);
    fo.rawWriteExact(fi.name[]);
  }

  // fix header
  fo.seek(4);
  fo.writeNum!uint(cast(uint)files.length);
  fo.writeNum!uint(dofs);
}

/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 * Partially based on Andy Noble's Manic Miner PC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module engine;

import x11gfx;
import wadarc;

__gshared bool debugColdet = false;
__gshared bool singleStep = false;
__gshared bool singleStepWait = false;


// ////////////////////////////////////////////////////////////////////////// //
private __gshared VColor[256] palette;
__gshared ubyte[][string] gameData;
__gshared Room[] gameRooms;
__gshared string gameLevelsName;

/*
  loadfile (GFXDATA "air", GFXair, 2048);
  loadfile (GFXDATA "fant", GFXfant, 8 * 96);
  loadfile (GFXDATA "final", GFXfinal, 256 * 64);
  loadfile (GFXDATA "fontb", fontb, 8 * 96);
  loadfile (GFXDATA "fonts", fonts, 6 * 64);
  loadfile (GFXDATA "house", GFXhouse, 1024);
  loadfile (GFXDATA "load", GFXload, 32 * 8 * 2);
  loadfile (GFXDATA "over", GFXover, 256 * 2);
  loadfile (GFXDATA "piano", GFXpiano, 256 * 64);
  loadfile (GFXDATA "pkeys", GFXpkeys, 128 * 8);
  loadfile (GFXDATA "switch", GFXswitch, 2 * 64);
  loadfile (GFXDATA "tplate", GFXtplate, 32 * 64);
  loadfile (GFXDATA "win", GFXwin, 4864);
*/


private void loadRooms (const(ubyte)[] data) {
  gameRooms.length = 0;
  if (data.length < 8) throw new Exception("invalid levels");
  if (data[0..8] != "MANIC\x0d\x0a\x1a") throw new Exception("invalid levels");
  data = data[8..$];
  if (data.length < 16) throw new Exception("invalid levels");
  char[16] lname = (cast(const(char)[])data)[0..16];
  data = data[16..$];
  const(char)[] nn;
  foreach (immutable idx, char ch; lname[]) {
    if (ch == 0) break;
    nn = lname[0..idx];
  }
  while (nn.length && nn[$-1] <= ' ') nn = nn[0..$-1];
  gameLevelsName = nn.idup;
  while (data.length > 0) {
    gameRooms.length += 1;
    gameRooms[$-1].roomIdx = cast(int)gameRooms.length-1;
    gameRooms[$-1].load(data);
  }
}


public void loadGameData () {
  {
    auto pp = wadLoadFile("palmain");
    foreach (immutable cc; 0..256) {
      ubyte r = cast(ubyte)(255*pp[cc*3+0]/63);
      ubyte g = cast(ubyte)(255*pp[cc*3+1]/63);
      ubyte b = cast(ubyte)(255*pp[cc*3+2]/63);
      palette[cc] = rgbcol(r, g, b);
    }
  }

  forEachWadFile(delegate (string name, uint size) {
    if (name == "palmain") return;
    auto buf = wadLoadFile(name);
    if (name == "levels") {
      loadRooms(buf);
    } else {
      gameData[name] = buf;
    }
  });

  if (gameRooms.length == 0) throw new Exception("no levels");
}


// ////////////////////////////////////////////////////////////////////////// //
// image builders
public VColor palcol (ubyte c) nothrow @trusted @nogc { pragma(inline, true); return palette[c]; }


// ////////////////////////////////////////////////////////////////////////// //
public X11Image buildImageMasked (const(ubyte)[] darr, int w, int h) {
  assert(w > 0 && h > 0);
  int dpos = 0;
  auto img = new X11Image(w, h);
  foreach (immutable y; 0..h) {
    foreach (immutable x; 0..w) {
      ubyte c = darr[dpos++];
      if (c) {
        img.setPixel(x, y, palcol(c));
      } else {
        img.setPixel(x, y, Transparent);
      }
    }
  }
  return img;
}


public X11Image buildImageMaskedShiny (const(ubyte)[] darr, int brightness, int w, int h) {
  assert(w > 0 && h > 0);
  int dpos = 0;
  auto img = new X11Image(w, h);
  foreach (immutable y; 0..h) {
    foreach (immutable x; 0..w) {
      ubyte c = darr[dpos++];
      if (c) {
        int b = (c&15)-brightness;
        if (b < 0) b = 0; else if (b > 15) b = 15;
        img.setPixel(x, y, palcol(cast(ubyte)((c&240)|b)));
      } else {
        img.setPixel(x, y, Transparent);
      }
    }
  }
  return img;
}


public X11Image buildImageMaskedInk (const(ubyte)[] darr, int ink, int w, int h) {
  assert(w > 0 && h > 0);
  int dpos = 0;
  auto img = new X11Image(w, h);
  if (ink < 0) ink = 0;
  ink *= 16;
  foreach (immutable y; 0..h) {
    foreach (immutable x; 0..w) {
      ubyte c = darr[dpos++];
      if (c) {
        img.setPixel(x, y, palcol(cast(ubyte)(c+ink)));
      } else {
        img.setPixel(x, y, Transparent);
      }
    }
  }
  return img;
}


// ////////////////////////////////////////////////////////////////////////// //
public X11Image buildImageBrick (const(ubyte)[] darr, int ink, int paper, int skipy=0) {
  assert(skipy >= 0);
  enum { w = 8, h = 8 }
  int dpos = 0;
  auto img = new X11Image(w, h);
  ink *= 16;
  foreach (immutable y; 0..h) {
    foreach (immutable x; 0..w) {
      ubyte c = (y >= skipy ? darr[dpos++] : 0);
      img.setPixel(x, y, palcol(cast(ubyte)(c ? ink+c : paper)));
    }
  }
  return img;
}


// ////////////////////////////////////////////////////////////////////////// //
// willy jump offsets
private:
static immutable int[19] willyJ = [0, -4, -4, -3, -3, -2, -2, -1, -1, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4];

struct SkyLabXY { int x, y; }
static immutable SkyLabXY[4][3] skylabCoords = [
  [{x: 8,y:0}, {x: 72,y:0}, {x:136,y:0}, {x:200,y:0}],
  [{x:40,y:0}, {x:104,y:0}, {x:168,y:0}, {x:232,y:0}],
  [{x:24,y:0}, {x: 88,y:0}, {x:152,y:0}, {x:216,y:0}]
];


// ////////////////////////////////////////////////////////////////////////// //
// suitable to "unjson"
public struct Room {
  private enum SRZIgnore;
  enum { Width = 32, Height = 16 }
  static struct WillyStart { int x; int y; int sd; }
  static struct ExitGfx { ubyte gfx; int x; int y; }
  static struct CommonGfx { ubyte gfx; ubyte ink; ubyte paper; }
  static struct Conveyor { int x; int y; int d; int l; ubyte gfx; ubyte ink; ubyte paper; @SRZIgnore int frame; }
  static struct KeyPos { int x; int y; int s; }
  static struct Key { ubyte gfx; KeyPos[] info; }
  static struct SwitchPos { int x; int y; int s; }
  static struct Enemy { bool vert; ushort gfx; ubyte ink; ubyte paper; int x=-1; int y; int min; int max; int d; int s; int flip; int anim; @SRZIgnore int frame; @SRZIgnore int m; @SRZIgnore int falling; @SRZIgnore int delay; }
  static struct SkyLabEnemy { int p, s; ubyte ink; int max; int m; int frame; }
  static struct SkyLabXY { int x, y, frame; ubyte ink; int m; int s; int max; int p; }
  static struct SPGRay { int x, y; }
  @SRZIgnore int roomIdx;
  string title;
  WillyStart willy;
  ExitGfx exit;
  int air;
  ubyte[Height*Width] map; // 16 rows, 32 cols
  ubyte border, ink, paper;
  CommonGfx[] platforms;
  CommonGfx wall;
  CommonGfx crumb;
  CommonGfx[] deadlies;
  Conveyor conveyor;
  Key keys;
  SwitchPos[] switches;
  Enemy[] enemies;

  @SRZIgnore int air8 = 8;

  @SRZIgnore Enemy eugene;

  @SRZIgnore int holeLen, holeY;
  @SRZIgnore Enemy kong;

  @SRZIgnore SkyLabEnemy[] skylabEnemies;
  @SRZIgnore SkyLabXY[] skylab;

  @SRZIgnore SPGRay[] spg; // solar power generator

  @SRZIgnore private {
    int willyX, willyY, willyDir, willyJump, willyJumpDir;
    int willyLastMoveDir, willyFall, willyConv, willyStall;
    public bool willyDead;
    int frameNo;
    public int score;
    // keys
    bool kLeft, kRight, kJump, kUp, kDown;
    bool kLeftDown, kRightDown, kJumpDown, kUpDown, kDownDown;
  }

  @SRZIgnore private {
    X11Image finalSpr;
    X11Image sunSpr;
    X11Image[] brickCache;
    X11Image[] convCache;
    X11Image[] exitCache;
    X11Image[] keyCache;
    X11Image[][] monsterCache;
    int[][] monsterOfsCache;
    X11Image[] eugeneSpr;
    X11Image[] kongSpr;
    X11Image[] skylabSpr;
    X11Image[] switchesSpr;
    X11Image willySpr;
    X11Image gfxAir;
  }

  ubyte saveKeyState () {
    return
      ((kLeftDown ? 1 : 0)<<0)|
      ((kRightDown ? 1 : 0)<<1)|
      ((kUpDown ? 1 : 0)<<2)|
      ((kDownDown ? 1 : 0)<<3)|
      ((kJumpDown ? 1 : 0)<<4)|
      0;
  }

  void restoreKeyState (ubyte b) {
    keyLeft = ((b&(1<<0)) != 0);
    keyRight = ((b&(1<<1)) != 0);
    keyUp = ((b&(1<<2)) != 0);
    keyDown = ((b&(1<<3)) != 0);
    keyJump = ((b&(1<<4)) != 0);
  }

  void initRoom () {
    void initWilly () {
      willyX = willy.x;
      willyY = willy.y;
      willyDir = (willy.sd > 0 ? -1 : 1);
      willyJump = willyFall = willyConv = willyStall = 0;
      willyDead = false;
      willyLastMoveDir = 0;
      kLeft = kRight = kJump = kUp = kDown = false;
      kLeftDown = kRightDown = kJumpDown = kUpDown = kDownDown = false;
    }

    buildWilly();
    buildBrickImages();
    buildConvImages();
    buildExitImages();
    buildKeysImages();
    buildMonsterImages();
    buildEugeneImages();
    buildKongImages();
    buildSkyLabImages();
    buildSwitchImages();

    finalSpr = buildImageMasked(gameData["final"], 256, 64);
    sunSpr = buildImageMasked(gameData["sun"], 24, 16);
    gfxAir = buildImageMasked(gameData["air"], 256, 8);

    initWilly();

    frameNo = 0;
    foreach (ref c; map) {
      if (c == 4) c = 8; // 'crumb'
      else if (c < 1 || c > 7) c = 0; // emptyness
    }

    // rebuild keys
    KeyPos[] kinfo;
    foreach (const ref ki; keys.info) {
      if (ki.s == 0) continue;
      kinfo ~= ki;
    }
    keys.info = kinfo;

    platforms = platforms.dup;
    deadlies = deadlies.dup;
    switches = switches.dup;
    enemies = enemies.dup;

    // Eugene?
    if (roomIdx == 4) {
      Enemy[] a = [{ x:120, y:1, d/*ir*/:0, min:1, max:87, ink:6 }];
      eugene = a[0];
    }
    // Kong?
    if (roomIdx == 7 || roomIdx == 11) {
      Enemy[] a = [{ x:120, y:0, max:104, frame:0, ink:2, m:0 }];
      kong = a[0];
      holeLen = 2*8;
      holeY = 0;
    }
    // SkyLab?
    if (roomIdx == 13) {
      enemies = null;
      SkyLabEnemy[] a = [
        {p:0, s:4, ink:6, max:72, m:0, frame:0},
        {p:2, s:3, ink:5, max:56, m:0, frame:0},
        {p:1, s:1, ink:4, max:32, m:0, frame:0}
      ];
      skylabEnemies = a.dup;
      foreach (immutable f, const ref se; skylabEnemies) {
        skylab ~= SkyLabXY(
          skylabCoords[f][se.p].x, skylabCoords[f][se.p].y,
          se.frame, se.ink, se.m, se.s, se.max, se.p,
        );
      }
    }
    // Solar Power Generator?
    if (roomIdx == 18) buildSPG();
  }

  void keyLeft (bool pressed) { if (pressed) kLeft = true; kLeftDown = pressed; }
  void keyRight (bool pressed) { if (pressed) kRight = true; kRightDown = pressed; }
  void keyUp (bool pressed) { if (pressed) kUp = true; kUpDown = pressed; }
  void keyDown (bool pressed) { if (pressed) kDown = true; kDownDown = pressed; }
  void keyJump (bool pressed) { if (pressed) kJump = true; kJumpDown = pressed; }

  void stepGame () {
    kLeft = kLeft||kLeftDown;
    kRight = kRight||kRightDown;
    kUp = kUp||kUpDown;
    kDown = kDown||kDownDown;
    kJump = kJump||kJumpDown;
    scope(exit) kLeft = kRight = kJump = kUp = kDown = false;

    ++frameNo;
    conveyor.frame += (conveyor.d ? -1 : 1);
    if (conveyor.frame < 0) conveyor.frame = 3; else if (conveyor.frame > 3) conveyor.frame = 0;

    if (!willyJump) stepCrumb();
    stepMonsters();
    stepWillyActions();
    stepKeys();
    stepSwitch();
    buildSPG();

    if (--air8 <= 0) {
      air8 = 8;
      if (--air <= 33) {
        air = 33;
        willyDead = true;
      }
    }
  }

  void cheatRemoveKeys () { keys.info = null; }

  // ////////////////////////////////////////////////////////////////////// //
  // checkers

  // x & y: in pixels
  ubyte getBlockAt (int x, int y, bool simplify=false) const nothrow @nogc {
    x = x>>3;
    y = y>>3;
    if (x < 0 || y < 0 || x > 31 || y > 15) return 0; // empty
    ubyte b = map[y*32+x];
    if (simplify) {
           if (b > 15) b = 0;
      else if (b > 7) b = 4;
      else if (b == 6) b = 5;
      else if (b == 2) b = 1;
    }
    return b;
  }

  bool checkExit () const nothrow @nogc {
    if (keys.info.length != 0) return false;
    auto x = exit.x;
    auto y = exit.y;
    return (willyX >= x-2 && willyX+10 <= x+18 && willyY >= y-5 && willyY+16 <= y+22);
  }

  bool checkMonsters () const nothrow {
    foreach (immutable f, const ref m; enemies) {
      auto cc = monsterOfsCache[f];
      if (m.x < 0 || m.y < 0) continue;
      if (m.vert) {
        if (pixelCheckMonster(m.x, m.y, gameData["vrobo"], cc[m.anim])) return true;
      } else {
        if (pixelCheckMonster(m.x&248, m.y, gameData["hrobo"], cc[((m.x&m.anim)&0xfe)+m.d/*ir*/])) return true;
      }
    }
    // Eugene?
    if (eugene.x >= 0) {
      enum curLSet = 0;
      if (pixelCheckMonster(eugene.x, eugene.y, gameData["eugene"], 256*curLSet)) return true;
    }
    // Kong?
    if (kong.x >= 0) {
      if (pixelCheckMonster(kong.x, kong.y, gameData["kong"], 256*kong.frame)) return true;
    }
    // SkyLab?
    if (skylab.length) {
      foreach (const ref sk; skylab) {
        if (pixelCheckMonster(sk.x, sk.y, gameData["sky"], 256*sk.frame)) return true;
      }
    }
    return false;
  }

  void checkSwitch () nothrow @nogc {
    foreach (ref ss; switches) {
      if (ss.s/*tate*/ != 1) continue;
      auto x = ss.x;
      auto y = ss.y;
      if (x+7 >= willyX && y+7 >= willyY && x < willyX+8 && y < willyY+16) ss.s/*tate*/ = 1+1;
    }
  }

  bool checkSPG () const nothrow @nogc {
    foreach (const ref sp; spg) {
      auto x = sp.x*8;
      auto y = sp.y*8;
      if (x < 0 || y < 0) break;
      if (x+7 >= willyX && x < willyX+8 && y+7 >= willyY && y < willyY+16) return true;
    }
    return false;
  }

  X11Image draw (X11Image img=null) {
    if (img is null) img = new X11Image(8*Room.Width, 8*Room.Height);
    int pos = 0;
    foreach (immutable y; 0..img.height) {
      foreach (immutable x; 0..img.width) {
        img.setPixel(x, y, palcol(this.paper));
      }
    }
    foreach (immutable y; 0..Room.Height) {
      foreach (immutable x; 0..Room.Width) {
        ubyte blk = this.map[pos++];
        if (blk < 1 || blk == 7) continue;
        if (blk == 4) blk = 8;
        if (blk < brickCache.length && brickCache[blk] !is null) {
          brickCache[blk].blitTo(img, x*8, y*8);
        }
      }
    }
    if (this.roomIdx == 19) {
      finalSpr.blitTo(img, 0, 0);
      sunSpr.blitTo(img, 60, 32);
    }

    void drawConveyor () {
      auto conv = &this.conveyor;
      if (conv.l < 1) return;
      auto y = conv.y;
      if (y <= 0) return;
      convCache[conv.frame].blitTo(img, conv.x, conv.y, conv.l);
    }

    void drawExit () {
      int br;
      if (this.keys.info.length != 0) {
        br = 0;
      } else {
        br = this.frameNo&31;
        if (br > 15) br = 31-br;
        ++br;
      }
      exitCache[br].blitTo(img, this.exit.x, this.exit.y);
    }

    void drawKeys () {
      auto ff = this.frameNo%16;
      if (ff >= 8) ff = 15-ff;
      auto keyimg = keyCache[ff];
      foreach_reverse (const ref ki; this.keys.info) {
        if (ki.x < 0 || ki.y < 0) continue;
        //eraseRect(ki.x, ki.y, 8, 8);
        if (!ki.s) continue;
        keyimg.blitTo(img, ki.x&248, ki.y);
      }
    }

    void drawMonsters () {
      foreach (immutable f, const ref m; this.enemies) {
        if (m.x < 0 || m.y < 0) continue;
        auto slist = monsterCache[f];
        auto x = m.x;
        if (m.vert) {
          //for (var c = 0; c <= m.anim; c++) r.push(buildImageMaskedInk(me.data.vrobo, (m.gfx+c)*256, m.ink-1, 16, 16, false));
          slist[m.anim].blitTo(img, x, m.y);
        } else {
          auto sidx = ((x&m.anim)&0xfe)+m.d/*ir*/;
          if (sidx < slist.length) {
            slist[sidx].blitTo(img, x&248, m.y);
          } else {
            import core.stdc.stdio : stderr, fprintf;
            stderr.fprintf("monster #%u is fucked: sidx=%u; max=%u", cast(uint)f, cast(uint)sidx, cast(uint)slist.length);
          }
        }
      }
      if (this.eugene.x >= 0) {
        enum curLSet = 0;
        eugeneSpr[curLSet*8+this.eugene.ink].blitTo(img, this.eugene.x, this.eugene.y);
      }
      if (this.kong.x >= 0) {
        kongSpr[this.kong.frame*8+this.kong.ink].blitTo(img, this.kong.x, this.kong.y);
      }
      if (this.skylab.length) {
        foreach (const ref sk; this.skylab) {
          skylabSpr[sk.frame*8+sk.ink].blitTo(img, sk.x, sk.y);
        }
      }
    }

    void drawSwitch () {
      foreach (const ref ss; this.switches) {
        if (ss.s == 0) continue;
        switchesSpr[ss.s-1].blitTo(img, ss.x, ss.y);
      }
    }

    void drawSPG () {
      foreach (const ref sp; this.spg) {
        auto x = sp.x*8;
        auto y = sp.y*8;
        if (x < 0 || y < 0) break;
        //ctx.fillStyle = mainPal[noerase?6:this.paper];
        //ctx.fillRect(x, y, 8, 8);
        foreach (immutable dy; 0..8) {
          foreach (immutable dx; 0..8) {
            img.setPixel(x+dx, y+dy, palcol(6));
          }
        }
      }
    }

    void drawWilly () {
      auto willyPos = (this.willyX&15)>>1;
      if (this.willyDir < 0) willyPos += 8;
      if (debugColdet) {
        auto wy = 0;
        if (this.checkMonsters()) wy = 16;
        willySpr.blitTo(willyPos*16, wy, 16, 16, img, this.willyX&248, this.willyY);
      } else {
        willySpr.blitTo(willyPos*16, 0, 16, 16, img, this.willyX&248, this.willyY);
      }
    }

    void drawAir () {
      gfxAir.blitFast(0, 136);
      printAt(1, 136, "AIR", 9);
      //for (int x = 3; x >= 0; --x) PlotXY (30, 138 + x, GFXair + 512 + 256 * x + 30, 226, 1);
      setPixel(32, 138, palcol(122));
      setPixel(32, 139, palcol(116));
      setPixel(32, 140, palcol(119));
      setPixel(32, 141, palcol(123));
      if (air > 33) {
        drawBar8(33, 138, air-33, 1, 120);
        drawBar8(33, 139, air-33, 1, 114);
        drawBar8(33, 140, air-33, 1, 117);
        drawBar8(33, 141, air-33, 1, 121);
      }
      if (air > 32) {
        setPixel(air-1, 138, palcol(122));
        setPixel(air-1, 139, palcol(116));
        setPixel(air-1, 140, palcol(119));
        setPixel(air-1, 141, palcol(123));
      }
    }

    drawConveyor();
    drawKeys();
    drawMonsters();
    drawSwitch();
    drawWilly();
    drawExit();
    drawSPG();

    drawAir();

    drawStr(128-cast(int)title.length*6/2, 128, title, rgbcol(255, 255, 0));

    return img;
  }

private:
  // ////////////////////////////////////////////////////////////////////// //
  // pixel-perfect collision detector
  // fully stolen from Andy's sources %-)
  bool pixelCheckMonster (int rx, int ry, const(ubyte)[] darr, int dpos=0) const nothrow {
    static ubyte[256] mpcGrid;
    assert(dpos >= 0);
    //int x, y, w;
    int x, w;
    rx -= willyX&248;
    ry -= willyY;
    if (rx < -15 || rx > 15 || ry < -15 || ry > 15) return false;
    // clear grid
    mpcGrid[] = 0;
    if (rx < 0) { x = 0; rx = -rx; w = 16-rx; } else { x = rx; rx = 0; w = 16-x; }
    // partial plot monster
    for (int y = ry+15; y >= ry; --y) {
      if (y >= 0 && y < 16) {
        int gp = y*16+x;
        int rp = dpos+(y-ry)*16+rx;
        for (int dx = 0; dx < w; ++dx, ++gp, ++rp) mpcGrid[gp] = darr[rp];
      }
    }
    auto warr = gameData["willy"];
    int wptr = ((willyX&15)>>1)*256+(willyDir < 0 ? 2048 : 0);
    // check for collision
    int mp = 0;
    for (x = 255; x >= 0; --x, ++wptr, ++mp) if (warr[wptr] && mpcGrid[mp]) return true;
    return false;
  }

  // ////////////////////////////////////////////////////////////////////// //
  bool isWillyFalling () const nothrow @nogc {
    if (willyY&7) return true;
    for (int dx = 0; dx <= 8; dx += 8) {
      ubyte b = getBlockAt(willyX+dx, willyY+16, true);
      if (b > 0 && b != 5) return false;
    }
    return true;
  }

  bool isWillyInDeadly () const nothrow @nogc {
    for (int dx = 0; dx <= 8; dx += 8) {
      for (int dy = 0; dy <= 16; dy += 8) {
        if (getBlockAt(willyX+dx, willyY+dy, true) == 5) return true;
      }
    }
    return false;
  }

  bool isWillyOnConv () const nothrow @nogc {
    if (willyY&7) return false;
    ubyte b0 = getBlockAt(willyX, willyY+16);
    ubyte b1 = getBlockAt(willyX+8, willyY+16);
    return (b0 == 7 || b1 == 7);
  }

  // ////////////////////////////////////////////////////////////////////// //
  // called on each frame
  void buildSPG (bool forced=false) {
    if (!forced && roomIdx != 18) {
      spg = null;
      return;
    }

    bool spgCheckMonster (int x, int y) {
      x *= 8;
      y *= 8;
      foreach (const ref cm; enemies) {
        auto mx = cm.x;
        auto my = cm.y;
        if (x+7 >= mx && x < mx+15 && y+7 >= my && y < my+15) return true;
      }
      return false;
    }

    int x = 23, y = 0;
    bool done = false;
    int dir = 0, idx = 0;

    if (spg.length) {
      spg.length = 0;
      spg.assumeSafeAppend;
    }

    void addXY (int x, int y) {
      ++idx;
      while (spg.length < idx) spg ~= SPGRay(-1, -1);
      spg[idx-1].x = x;
      spg[idx-1].y = y;
    }

    do {
      ubyte blockhit = map[y*32+x];
      bool robohit = spgCheckMonster(x, y);

      if (blockhit && robohit) {
        addXY(-1, -1);
        done = true;
      } else if (!blockhit && robohit) {
        if (idx && spg[idx-1].x == x && spg[idx-1].y == y) {
          spg[idx-1].x = spg[idx-1].y = -1;
          done = true;
        } else {
          addXY(x, y);
        }
        dir ^= 1;
      } else if (!blockhit && !robohit) {
        addXY(x, y);
      } else if (blockhit && !robohit) {
        if (idx && spg[idx-1].x == x && spg[idx-1].y == y) {
          spg[idx-1].x = spg[idx-1].y = -1;
          done = true;
        }
        dir ^= 1;
      }

      if (!blockhit) {
        if (!dir) {
          ++y;
          blockhit = map[y*32+x];
          if (y == 15 || blockhit) done = true;
        } else {
          --x;
          blockhit = map[y*32+x];
          if (x == 0 || blockhit) done = true;
        }
      } else {
        if (!dir) { --x; if (!x) done = true; }
        else { ++y; if (++y == 15) done = true; }
      }
    } while (!done);
    addXY(-1, -1);
  }

  void stepMonsters () {
    foreach (ref m; enemies) {
      if (m.x < 0 || m.y < 0) continue;
      if (m.vert) {
        auto y = m.y;
        auto spd = m.s/*peed*/;
        if (m.d/*ir*/ != 0) {
          // up
          y -= spd;
          if (y < m.min || y < 0) { y += spd; m.d/*ir*/ = 0; }
        } else {
          // down
          y += spd;
          if (y > m.max) { y -= spd; m.d/*ir*/ = 1; }
        }
        m.y = y;
        m.anim = (m.anim+1)&3;
      } else {
        auto x = m.x;
        auto spd = (2>>m.s/*peed*/);
        if (m.d/*ir*/ != 0) {
          // left
          x -= spd;
          if (x < m.min) { m.d/*ir*/ = 0; x += spd; }
        } else {
          // right
          x += spd;
          if (x > m.max+6) { m.d/*ir*/ = 1; x -= spd; }
        }
        m.x = x;
      }
    }
    // Eugene
    if (eugene.x >= 0) {
      if (keys.info.length == 0) {
        // no keys, Eugene tries to block the exit
        eugene.ink = (eugene.ink+1)&7;
        eugene.y += (eugene.y < eugene.max ? 1 : 0);
      } else {
        if (eugene.d/*ir*/ != 0) {
          // up
          --eugene.y;
          if (eugene.y < eugene.min) { eugene.d/*ir*/ = 0; ++eugene.y; }
        } else {
          // down
          ++eugene.y;
          if (eugene.y > eugene.max) { eugene.d/*ir*/ = 1; --eugene.y; }
        }
      }
    }
    // Kong
    if (kong.x >= 0) {
      switch (kong.falling) {
        case 1: // just started
          map[2*32+15] = 0;
          map[2*32+16] = 0;
          //eraseRect(16, 120, 16, 8);
          kong.falling = 2;
          break;
        case 2:
          kong.ink = 4;
          kong.frame += 2;
          kong.falling = 3;
          break;
        case 3:
          kong.y += 4;
          if (kong.y >= kong.max) { kong.x = -1; score += 100; }
          if (!kong.delay) { kong.delay = 4; kong.frame = ((kong.frame-1)&1)+2; } else --kong.delay;
          break;
        default:
          if (!kong.delay) { kong.delay = 8; kong.frame = (kong.frame+1)&1; } else --kong.delay;
          break;
      }
    }
    // SkyLab
    foreach (immutable idx, ref sk; skylab) {
      switch (sk.m) {
        case 0:
          sk.y += sk.s;
          if (sk.y > sk.max) {
            sk.y = sk.max;
            sk.m = 1;
            ++sk.frame;
          }
          break;
        case 1:
          ++sk.frame;
          if (sk.frame == 7) sk.m = 2;
          break;
        case 2:
          sk.p = (sk.p+1)&3;
          sk.x = skylabCoords[idx][sk.p].x;
          sk.y = skylabCoords[idx][sk.p].y;
          sk.frame = sk.m = 0;
          break;
        default:
      }
    }
  }

  void stepCrumb () {
    if (willyY&7) return;
    for (int f = 0; f <= 8; f += 8) {
      int x = willyX+f;
      int y = willyY+16;
      ubyte b = getBlockAt(x, y);
      if (b == 4) b = 8;
      if (b < 8) continue;
      x >>= 3;
      y >>= 3;
      if (++b > 15) b = 0;
      map[y*32+x] = b;
      //eraseRect(x*8, y*8, 8, 8);
    }
  }

  void stepKeys () {
    while (keys.info.length) {
      bool again = false;
      foreach (immutable idx; 0..keys.info.length) {
        auto ki = &keys.info[idx];
        assert(ki.s);
        int kx = ki.x;
        int ky = ki.y;
        if (kx+7 >= willyX && kx < willyX+10 && ky+7 >= willyY && ky < willyY+16) {
          score += 100;
          foreach (immutable c; idx+1..keys.info.length) keys.info[c-1] = keys.info[c];
          keys.info.length -= 1;
          again = true;
          break;
        }
      }
      if (!again) break;
    }
  }

  void stepSwitch () {
    // hole?
    if (holeLen > 0 && switches.length && switches[0].s/*tate*/ > 1) {
      if (holeLen < 0) {
        //FIXME
        enemies[1].max += 24;
        holeLen = 0;
      } else {
        ++holeY;
        if (holeY == 16) {
          map[11*32+17] = 0;
          map[12*32+17] = 0;
          holeLen = -1;
        }
      }
    }
    // Kong?
    if (kong.x >= 0 && !kong.falling && switches.length > 1 && switches[1].s/*tate*/ > 1) kong.falling = 1;
  }

  void stepWillyActions () {
    bool doWillyLeft () {
      if (willyDir > 0) { willyDir = -1; return true; }
      auto xx = willyX-2;
      auto b0 = getBlockAt(xx, willyY);
      auto b1 = getBlockAt(xx, willyY+8);
      auto b2 = (willyY&7 ? getBlockAt(xx, willyY+16) : b1);
      if (b0 == 3 || b1 == 3 || b2 == 3) return false;
      willyX -= 2;
      if (willyX < 0) willyX += 240;
      willyLastMoveDir = -1;
      return true;
    }

    bool doWillyRight () {
      if (willyDir < 0) { willyDir = 1; return true; }
      if (willyX > 245) return false;
      auto xx = willyX+10;
      auto b0 = getBlockAt(xx, willyY);
      auto b1 = getBlockAt(xx, willyY+8);
      auto b2 = (willyY&7 ? getBlockAt(xx, willyY+16) : b1);
      if (b0 == 3 || b1 == 3 || b2 == 3) return false;
      willyX += 2;
      if (willyX > 240) willyX -= 240;
      willyLastMoveDir = 1;
      return true;
    }

    void doWillyJump () {
      if (!willyJump) return;
      willyY += willyJ[willyJump];
      auto x = willyX;
      auto mv = false;
      if (willyJumpDir < 0) mv = doWillyLeft(); else if (willyJumpDir > 0) mv = doWillyRight();
      if (willyJump < 9) {
        willyFall = 0;
        // up
        auto b0 = getBlockAt(x, willyY);
        auto b1 = getBlockAt(x+8, willyY);
        if (b0 == 3 || b1 == 3) {
          // headboom! (apstenu %-)
          willyX = x;
          willyY -= willyJ[willyJump];
          willyJump = 0; // enough flying
          return;
        }
      } else {
        // down
        if (willyJump > 12) willyFall += willyJ[willyJump];
        if ((willyY&7) == 0) {
          auto b0 = getBlockAt(willyX, willyY+16);
          auto b1 = getBlockAt(willyX+8, willyY+16);
          if (b0 || b1) {
            if (b0 == 3 || b1 == 3) willyX = x;
            willyFall = 0; // can't fall too deep while jumping
            willyJump = 0; // enough flying
            if (b0 == 7 || b1 == 7) willyStall = 1; // conveyor?
            return;
          }
        }
      }
      ++willyJump;
      if (willyJump > 18) willyJump = 0;
    }


    if (willyDead) return;
    checkSwitch();
    if (isWillyInDeadly()) { willyDead = true; return; }
    if (!debugColdet) {
      if (checkMonsters()) { willyDead = true; return; }
    } else {
      if (checkMonsters()) singleStep = singleStepWait = true;
    }

    auto wasJump = false;
    if (willyJump) {
      willyLastMoveDir = 0;
      doWillyJump();
      if (willyJump) return;
      wasJump = true;
    }

    auto falling = isWillyFalling();
    if (!kDown && falling) {
      willyConv = willyStall = willyLastMoveDir = 0;
      willyFall += 4;
      willyY += 4;
      if (willyY > 112) willyY -= 112;
      return;
    }

    if (!falling && willyFall > 34) willyDead = true; // too high!
    auto lfall = willyFall;
    willyFall = 0;

    if (willyDead) return;

    auto dx = (kLeft ? -1 : kRight ? 1 : 0);
    if (isWillyOnConv()) {
      auto cdir = (conveyor.d ? 1 : -1);
      //dx==cdir,!dx,lastmove==cdir
      if (willyLastMoveDir == cdir || dx == cdir || !dx) { willyConv = cdir; willyStall = 0; } // was moving in conv. dir or standing
      if (!willyConv) {
        // Willy just steps on the conveyor, and Willy walking to the opposite side
        willyStall = 0;
        if (wasJump && willyLastMoveDir == -cdir) {
          willyConv = dx; // from jump, can do opposite
        } else {
          willyConv = dx;
          if (lfall > 0 || !willyLastMoveDir) { dx = 0; willyStall = 1; } // lands on conveyor, not from jump
        }
      } else {
        // Willy was on conveyor
        dx = (willyStall ? 0 : willyConv);
      }
    } else {
      willyConv = willyStall = 0;
    }

    //if (willyConv) dx = willyConv;
    if (kUp && !wasJump) {
      willyConv = willyStall = willyLastMoveDir = 0;
      willyJumpDir = dx;
      willyJump = 1;
      doWillyJump();
      return;
    }
    if (kDown) willyY -= 8;
    willyLastMoveDir = 0;
    if (dx < 0) doWillyLeft(); else if (dx > 0) doWillyRight();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void buildWilly () {
    auto img = new X11Image(16*16, 16+16);
    auto ww = gameData["willy"];
    foreach (immutable f; 0..16) {
      foreach (immutable y; 0..16) {
        foreach (immutable x; 0..16) {
          ubyte c = ww[f*256+y*16+x];
          if (!c) {
            img.setPixel(f*16+x, y, Transparent);
          } else {
            img.setPixel(f*16+x, y, palcol(c));
            // white
            img.setPixel(f*16+x, y+16, palcol(15));
          }
        }
      }
    }
    willySpr = img;
  }

  void buildBrickImages () {
    auto buildBrickImage (in ref Room.CommonGfx brk, int skipy=0) {
      return buildImageBrick(gameData["blocks"][brk.gfx*64..$], brk.ink, this.paper, skipy);
    }
    brickCache = null;
    foreach (immutable f; 0..8) {
      X11Image img;
      switch (f) {
        //case 0: case 7: img = buildBrickImage(this.wall, 16); break;
        case 1: img = buildBrickImage(this.platforms[0]); break;
        case 2: img = buildBrickImage(this.platforms[1]); break;
        case 3: img = buildBrickImage(this.wall); break;
        //case 4: img = buildBrickImage(this.crumb); break;
        case 5: img = buildBrickImage(this.deadlies[0]); break;
        case 6: img = buildBrickImage(this.deadlies[1]); break;
        default:
      }
      brickCache ~= img;
    }
    foreach (immutable f; 0..9) brickCache ~= buildBrickImage(this.crumb, f);
  }

  void buildConvImages () {
    convCache = null;
    auto conv = &this.conveyor;
    if (conv.y <= 0 || conv.l < 1) return;
    foreach (immutable f; 0..4) {
      convCache ~= buildImageBrick(gameData["conv"][conv.gfx*256+f*64..$], conv.ink, this.paper);
    }
  }

  void buildExitImages () {
    exitCache = null;
    exitCache ~= buildImageMasked(gameData["exits"][this.exit.gfx*256..$], 16, 16);
    foreach (immutable f; 0..16) exitCache ~= buildImageMaskedShiny(gameData["exits"][this.exit.gfx*256..$], f, 16, 16);
  }

  void buildKeysImages () {
    keyCache = null;
    foreach (immutable f; 0..16) keyCache ~= buildImageMaskedShiny(gameData["keys"][this.keys.gfx*64..$], f, 8, 8);
  }

  void buildMonsterImages () {
    monsterCache = null;
    monsterOfsCache = null;
    foreach (immutable f, const ref m; this.enemies) {
      if (m.x < 0 || m.y < 0) {
        monsterOfsCache ~= null;
        monsterCache ~= null;
        continue;
      }
      X11Image[] r;
      int[] cc;
      if (m.vert) {
        foreach (immutable c; 0..4) {
          auto n = (m.gfx+c)*256;
          cc ~= n;
          r ~= buildImageMaskedInk(gameData["vrobo"][n..$], m.ink-1, 16, 16);
        }
      } else {
        foreach (immutable c; 0..(m.anim>>1)+1) {
          auto n = (m.gfx+c)*256;
          cc ~= n;
          r ~= buildImageMaskedInk(gameData["hrobo"][n..$], m.ink-1, 16, 16);
          n += m.flip*256;
          cc ~= n;
          r ~= buildImageMaskedInk(gameData["hrobo"][n..$], m.ink-1, 16, 16);
        }
      }
      monsterOfsCache ~= cc;
      monsterCache ~= r;
    }
  }

  void buildEugeneImages () {
    eugeneSpr = null;
    for (int f = 0; f <= 256; f += 256) {
      foreach (immutable c; 0..8) {
        eugeneSpr ~= buildImageMaskedInk(gameData["eugene"][f..$], c, 16, 16);
      }
    }
  }

  void buildKongImages () {
    kongSpr = null;
    foreach (immutable f; 0..4) {
      foreach (immutable c; 0..8) {
        kongSpr ~= buildImageMaskedInk(gameData["kong"][f*256..$], c, 16, 16);
      }
    }
  }

  void buildSkyLabImages () {
    skylabSpr = null;
    foreach (immutable f; 0..8) {
      foreach (immutable c; 0..8) {
        skylabSpr ~= buildImageMaskedInk(gameData["sky"][f*256..$], c, 16, 16);
      }
    }
  }

  void buildSwitchImages () {
    switchesSpr = null;
    ubyte[] swg;
    if (auto pp = "switches" in gameData) swg = *pp; else swg = gameData["switch"];
    foreach (immutable f; 0..2) {
      switchesSpr ~= buildImageBrick(swg[f*64..$], this.platforms[1].ink, this.paper);
    }
  }

private:
  void load (ref const(ubyte)[] data) {
    void readBuf (void[] buf) {
      import core.stdc.string : memcpy;
      if (buf.length == 0) return;
      if (data.length < buf.length) throw new Exception("invalid level");
      memcpy(buf.ptr, data.ptr, buf.length);
      data = data[buf.length..$];
    }

    ubyte readU8 () {
      if (data.length < 1) throw new Exception("invalid level");
      ubyte res = data[0];
      data = data[1..$];
      return res;
    }

    short readS16 () {
      if (data.length < 2) throw new Exception("invalid level");
      short res = cast(short)(data[0]|(data[1]<<8));
      data = data[2..$];
      return res;
    }

    char[33] title;
    readBuf(map[]);
    readBuf(title[]);
    const(char)[] tit;
    foreach (immutable idx, char ch; title[]) {
      if (ch == 0) break;
      tit = title[0..idx];
    }
    while (tit.length && tit[0] <= ' ') tit = tit[1..$];
    while (tit.length && tit[$-1] <= ' ') tit = tit[0..$-1];
    this.title = tit.idup;

    border = readU8;
    ink = readU8;
    paper = readU8;

    platforms.length = 2;
    foreach (immutable idx; 0..2) {
      CommonGfx g;
      g.ink = readU8;
      g.paper = readU8;
      g.gfx = readU8;
      platforms[idx] = g;
    }

    wall.ink = readU8;
    wall.paper = readU8;
    wall.gfx = readU8;

    crumb.ink = readU8;
    crumb.paper = readU8;
    crumb.gfx = readU8;

    deadlies.length = 2;
    foreach (immutable idx; 0..2) {
      CommonGfx g;
      g.ink = readU8;
      g.paper = readU8;
      g.gfx = readU8;
      deadlies[idx] = g;
    }

    conveyor.ink = readU8;
    conveyor.paper = readU8;
    conveyor.gfx = readU8;

    willy.x = readS16;
    willy.y = readS16;
    willy.sd = readU8;

    conveyor.x = readS16;
    conveyor.y = readS16;
    conveyor.d = readU8;
    conveyor.l = readU8;

    keys.info.length = 5;
    foreach (immutable idx; 0..5) keys.info[idx].x = readS16;
    foreach (immutable idx; 0..5) keys.info[idx].y = readS16;
    keys.gfx = readU8;
    foreach (immutable idx; 0..5) keys.info[idx].s = readU8;

    switches.length = 2;
    foreach (immutable idx; 0..2) switches[idx].x = readS16;
    foreach (immutable idx; 0..2) switches[idx].y = readS16;
    foreach (immutable idx; 0..2) switches[idx].s = readU8;

    exit.x = readS16;
    exit.y = readS16;
    exit.gfx = readU8;

    air = readU8;

    enemies.length = 4*2;

    foreach (immutable idx; 0..4) enemies[idx].vert = false;
    foreach (immutable idx; 0..4) enemies[idx].ink = readU8;
    foreach (immutable idx; 0..4) enemies[idx].paper = readU8;
    foreach (immutable idx; 0..4) enemies[idx].x = readS16;
    foreach (immutable idx; 0..4) enemies[idx].y = readS16;
    foreach (immutable idx; 0..4) enemies[idx].min = readS16;
    foreach (immutable idx; 0..4) enemies[idx].max = readS16;
    foreach (immutable idx; 0..4) enemies[idx].d = readU8;
    foreach (immutable idx; 0..4) enemies[idx].s = readU8;
    foreach (immutable idx; 0..4) enemies[idx].gfx = readS16;
    foreach (immutable idx; 0..4) enemies[idx].flip = readU8;
    foreach (immutable idx; 0..4) enemies[idx].anim = readU8;

    foreach (immutable idx; 4..8) enemies[idx].vert = true;
    foreach (immutable idx; 4..8) enemies[idx].ink = readU8;
    foreach (immutable idx; 4..8) enemies[idx].paper = readU8;
    foreach (immutable idx; 4..8) enemies[idx].x = readS16;
    foreach (immutable idx; 4..8) enemies[idx].y = readS16;
    foreach (immutable idx; 4..8) enemies[idx].min = readS16;
    foreach (immutable idx; 4..8) enemies[idx].max = readS16;
    foreach (immutable idx; 4..8) enemies[idx].d = readU8;
    foreach (immutable idx; 4..8) enemies[idx].s = readU8;
    foreach (immutable idx; 4..8) enemies[idx].gfx = readS16;
    foreach (immutable idx; 4..8) enemies[idx].anim = readU8;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void blitTo (X11Image src, X11Image dst, int x, int y, int repx=1) {
  foreach (immutable r; 0..repx) {
    foreach (immutable dy; 0..src.height) {
      foreach (immutable dx; 0..src.width) {
        int xx = x+dx;
        int yy = y+dy;
        if (xx < 0 || yy < 0 || xx >= dst.width || yy >= dst.height) continue;
        auto c = src.getPixel(dx, dy);
        if (isTransparent(c)) continue;
        dst.setPixel(xx, yy, c);
      }
    }
    x += src.width;
  }
}


public void blitTo (X11Image src, int x0, int y0, int ww, int hh, X11Image dst, int x, int y) {
  foreach (immutable dy; 0..ww) {
    foreach (immutable dx; 0..hh) {
      int xx = x+dx;
      int yy = y+dy;
      if (xx < 0 || yy < 0 || xx >= dst.width || yy >= dst.height) continue;
      auto c = src.getPixel(x0+dx, y0+dy);
      if (isTransparent(c)) continue;
      dst.setPixel(xx, yy, c);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void setPixel8 (int x, int y, ubyte col) {
  setPixel(x, y, palcol(col));
}


public void drawBar8 (int x, int y, int w, int h, ubyte col) {
  foreach (immutable dy; 0..h) {
    foreach (immutable dx; 0..w) {
      setPixel(x+dx, y+dy, palcol(col));
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void drawWilly4 (int ofs) {
  auto gw = gameData["willy"][256*ofs..$];
  enum x = 232, y = 72;
  foreach (immutable dy; 0..16) {
    foreach (immutable dx; 0..16) {
      ubyte c = gw[0];
      gw = gw[1..$];
      setPixel8(x+dx, y+dy, (c ? c+0*16 : 39));
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void printAt (int x, int y, const(char)[] text, ubyte col) {
  if (text.length == 0) return;

  int count = 0, count2, count3;
  int currentx, currenty;
  const(ubyte)* fonty, fonty2;

  /*
  currentx = ((x&127)*8)|((x&128) ? 4 : 0);
  currenty = ((y&127)*8)|((y&128) ? 4 : 0);
  { import std.stdio; writeln(currentx, "; ", currenty); }
  */
  currentx = x;
  currenty = y;

  auto gfxFant = gameData["fant"];
  auto fontb = gameData["fontb"];

  while (text.length) {
    int alpha = text[0];
    text = text[1..$];
    if (alpha == 170) {
      if (text.length == 0) break;
      currentx = 0;
      currenty += 8;
      alpha = text[0];
      text = text[1..$];
    }
    alpha -= 32;
    if (++count == 33) return;

    fonty = fontb.ptr+alpha*8;
    fonty2 = gfxFant.ptr+alpha*8;

    y = currenty;
    for (count2 = 0; count2 < 8; ++count2) {
      x = currentx;
      for (count3 = 0; count3 < 8; ++count3) {
        if (fonty[0]&(1<<count3)) {
          setPixel(x, y, palcol(col));
        } else if (fonty2[0]&(1<<count3)) {
          /*
          BYTE data = mm_gfx_getpixel (x, y);
          BYTE data2 = (data & 15) + 3;
          setPixel(x, y, palcol((data&240)|(data2 > 15 ? 15 : data2)));
          */
          putPixel(x, y, palcol(col)|(0x7f<<vlAShift));
          //ubyte data2 = 15;
          //setPixel(x, y, palcol(data2 > 15 ? 15 : data2));
        }
        //setPixel(x, y, rgbcol(255, 127, 0));
        ++x;
      }
      ++fonty;
      ++fonty2;
      ++y;
    }
    currentx += 8;
  }
}
